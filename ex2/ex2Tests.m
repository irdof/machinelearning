%% init
% Load Data
% The first two columns contain the exam scores and the third column contains the label.
data = load('ex2data1.txt');
X = data(:, [1, 2]); 
y = data(:, 3);
%%
%  Setup the data matrix appropriately
[m, n] = size(X);

% Add intercept term to X
X = [ones(m, 1) X];

% Initialize the fitting parameters
initial_theta = zeros(n + 1, 1);

%% ex1.1
sigmoid(0);

%% ex 1.2
for i = 0:100000
    alpha = 0.01;       
    [cost, grad] = costFunction(initial_theta, X, y);
    initial_theta = initial_theta - alpha * grad;
    if mod(i,1000) 
        fprintf('Cost at initial theta (zeros): %f\n', cost);
        disp('Gradient at initial theta (zeros):'); 
        disp(grad);
    end

end

%% ex 2.1
%  The first two columns contains the X values and the third column
%  contains the label (y).
data = load('ex2data2.txt');
X = data(:, [1, 2]); y = data(:, 3);

plotData(X, y);
% Put some labels 
hold on;
% Labels and Legend
xlabel('Microchip Test 1')
ylabel('Microchip Test 2')
% Specified in plot order
legend('y = 1', 'y = 0')
% Add Polynomial Features
% Note that mapFeature also adds a column of ones for us, so the intercept term is handled
X = mapFeature(X(:,1), X(:,2));
hold off;
